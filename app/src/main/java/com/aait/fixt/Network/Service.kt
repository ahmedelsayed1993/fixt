package com.aait.fixt.Network


import com.aait.fixt.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {

    @POST("phone-keys")
    fun Keys(@Query("lang") lang:String): Call<ListResponse>

    @POST("sign-up")
    fun SignUp(@Query("user_type") user_type:String,
               @Query("name") name:String,
               @Query("phone") phone:String,
               @Query("phone_key") phone_key:String,
               @Query("email") email:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<AboutResponse>
    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang: String):Call<UserResponse>
    @POST("resend-code")
    fun resend(@Query("user_id") user_id:Int,
               @Query("lang") lang:String):Call<BaseResponse>
    @POST("sign-in")
    fun login(@Query("lang") lang:String,
              @Query("phone") phone:String,
              @Query("password") password:String,
              @Query("device_id") device_id:String,
              @Query("device_type") device_type:String):Call<UserResponse>

    @POST("forget-password")
    fun forgotPass(@Query("phone") phone:String,
                   @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun updatePass(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("password") password:String,
                   @Query("code") code:String):Call<UserResponse>

    @POST("vehicles")
    fun getVehicles(@Query("lang") lang: String):Call<VehiclesResponse>

    @POST("models")
    fun getModels(@Query("lang") lang: String,
                  @Query("vehicle_id") vehicle_id:Int):Call<VehiclesResponse>

    @POST("engines")
    fun getEngines(@Query("lang") lang: String,
                   @Query("model_id") model_id:Int):Call<VehiclesResponse>

    @POST("add-vehicle")
    fun addVehicle(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("Vehicle_id") Vehicle_id:Int,
                   @Query("model_id") model_id:Int,
                   @Query("engine_id") engine_id:Int,
                   @Query("year") year:Int):Call<BaseResponse>
    @Multipart
    @POST("add-vehicle")
    fun addVehiclewithImage(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("Vehicle_id") Vehicle_id:Int,
                   @Query("model_id") model_id:Int,
                   @Query("engine_id") engine_id:Int,
                   @Query("year") year:Int,
                     @Part image_form:MultipartBody.Part):Call<BaseResponse>

    @POST("my-vehicles")
    fun myVehicles(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int):Call<MyVehiclesResponse>
    @POST("edit-profile")
    fun editProfile(@Query("lang") lang:String,
                    @Query("user_id") user_id:Int,
                    @Query("name") name:String?,
                    @Query("phone") phone:String?,
                    @Query("email") email: String?):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun editAvatar(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Part avatar:MultipartBody.Part):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id:Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("oil-types")
    fun getOilTypes(@Query("lang") lang: String):Call<VehiclesResponse>

    @POST("oils")
    fun getOils(@Query("lang") lang: String):Call<VehiclesResponse>
    @POST("types-washing")
    fun getWash_types(@Query("lang") lang:String):Call<VehiclesResponse>

    @POST("viscosities")
    fun getViscosity(@Query("lang") lang:String,
                     @Query("type_id") type_id:Int):Call<VehiclesResponse>

    @POST("add-order")
    fun addWashOrder(@Query("lang") lang:String,
                     @Query("user_id") user_id: Int,
                     @Query("car_id") car_id:Int,
                     @Query("order_type") order_type:String,
                     @Query("lat") lat:String,
                     @Query("lng") lng:String,
                     @Query("address") address:String,
                     @Query("date") date:String,
                     @Query("time") time:String,
                     @Query("wash_type") wash_type:Int,
                     @Query("have_oil") have_oil:Int
                     ):Call<AddOrderResponse>
    @POST("invoice-details")
    fun invoice(@Query("lang") lang:String,
                @Query("user_id") user_id: Int,
                @Query("order_id") order_id:Int):Call<InvoiceResponse>

    @POST("order-update")
    fun washUpdate(@Query("lang") lang:String,
                   @Query("order_id") order_id: Int,
                   @Query("user_id") user_id: Int,
                   @Query("lat") lat:String,
                   @Query("lng") lng: String,
                   @Query("address") address: String,
                   @Query("payment_type") payment_type:String?,
                   @Query("wash_type") wash_type:Int?,
                   @Query("discount_code") discount_code:String?,
                   @Query("have_oil") have_oil:Int):Call<InvoiceResponse>

    @POST("add-order")
    fun addOilOrder(@Query("lang") lang:String,
                    @Query("user_id") user_id:Int,
                    @Query("car_id") car_id: Int,
                    @Query("order_type") order_type:String,
                    @Query("lat") lat:String,
                    @Query("lng") lng:String,
                    @Query("address") address:String,
                    @Query("date") date:String,
                    @Query("time") time:String,
                    @Query("oil_id") oil_id:Int,
                    @Query("number_cans") number_cans:String,
                    @Query("oil_type_id") oil_type_id:Int,
                    @Query("viscosity_id") viscosity_id:Int,
                    @Query("oil_filter") oil_filter:String?,
                    @Query("filter_makina") filter_makina:String?,
                    @Query("filter_air") filter_air:String?,
                    @Query("have_oil") have_oil:Int
                    ):Call<AddOrderResponse>
    @POST("order-update")
    fun OilUpdate(@Query("lang") lang:String,
                   @Query("order_id") order_id: Int,
                   @Query("user_id") user_id: Int,
                   @Query("lat") lat:String,
                   @Query("lng") lng: String,
                   @Query("address") address: String,
                   @Query("payment_type") payment_type:String?,
                   @Query("filter_air") filter_air:String?,
                  @Query("filter_makina") filter_makina:String?,
                  @Query("oil_filter") oil_filter:String?,
                  @Query("number_cans") number_cans:String,
                  @Query("oil_type_id") oil_type_id:Int?,
                  @Query("have_oil") have_oil:Int?,
                  @Query("viscosity_id") viscosity_id:Int?,
                  @Query("oil_id") oil_id:Int?,
                   @Query("discount_code") discount_code:String?):Call<InvoiceResponse>

    @POST("about")
    fun About(@Query("lang") lang:String):Call<AboutResponse>
    @POST("washing")
    fun washing(@Query("lang") lang:String):Call<AboutResponse>

    @POST("my-orders")
    fun myOrder(@Query("lang") lang:String,
                @Query("user_id") user_id: Int,
                @Query("status") status:String):Call<OrdersResponse>
    @POST("number-orders")
    fun number(@Query("lang") lang:String,
               @Query("user_id") user_id:Int):Call<AddOrderResponse>

    @POST("details-car")
    fun getCar(@Query("lang") lang:String,@Query("user_id") user_id:Int,@Query("car_id") car_id:Int):Call<CarDetails>

    @POST("contact")
    fun getContact(@Query("lang") lang: String):Call<ContactResponse>

    @POST("notifications")
    fun getNotification(@Query("user_id") user_id: Int,
                        @Query("lang") lang:String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(@Query("lang") lang: String,
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<AboutResponse>

    @POST("discount-code")
    fun discount(@Query("lang") lang: String,
                 @Query("user_id") user_id:Int,
                 @Query("order_id") order_id:Int,
                 @Query("discount_code") discount_code:String):Call<CodeResponse>

    @POST("available-range")
    fun Avaailable(@Query("lang") lang:String,
                   @Query("user_id") user_id: Int,
                   @Query("order_type") order_type:String,
                   @Query("lat") lat:String,
                   @Query("lng") lng:String):Call<AboutResponse>
}