package com.aait.fixt.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.TimePicker
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil

class TimeActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_time
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var confirm:Button
    lateinit var start:TextView
    lateinit var end:TextView
    var time = ""
    var starting = 0
    var ending = 0


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        confirm = findViewById(R.id.confirm)
        start = findViewById(R.id.start)
        end = findViewById(R.id.end)
        back.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Determine_the_time)
        OnClickTime()
        confirm.setOnClickListener {
            if (CommonUtil.checkTextError(start,getString(R.string.start))||
                    CommonUtil.checkTextError(end,getString(R.string.end))){
                return@setOnClickListener

            }else {
                if (ending<starting||ending==starting){
                    CommonUtil.makeToast(mContext,getString(R.string.start_greater_end))
                }else {
                    val returnIntent = Intent()
                    returnIntent.putExtra("time", start.text.toString()+"-"+end.text.toString())
                    setResult(2, returnIntent)
                    finish()
                }
            }
        }


    }
    private fun OnClickTime() {

        val timePicker = findViewById<TimePicker>(R.id.timePicker)
        val timePicker1 = findViewById<TimePicker>(R.id.timePicker1)
        timePicker.setOnTimeChangedListener { _, hour, minute ->
            starting = hour
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }

            val hours = if (hour < 10) "0" + hour else hour
            start.text = " $hours $am_pm"
            Log.e("start",starting.toString())


        }
        timePicker1.setOnTimeChangedListener { _, hour, minute ->
            ending = hour
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }

            val hours = if (hour < 10) "0" + hour else hour
            val min = if (minute < 10) "0" + minute else minute
            // display format of time

            end.text = " $hours $am_pm"
            Log.e("end",ending.toString())


        }
        time = start.text.toString()+"-"+end.text.toString()
        Log.e("time",time)

    }
}