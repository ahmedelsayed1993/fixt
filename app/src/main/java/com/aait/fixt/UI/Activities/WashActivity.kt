package com.aait.fixt.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.AddOrderResponse
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Models.VehicleModel
import com.aait.fixt.Models.VehiclesResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Adapters.ListsAdapter
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WashActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        card_type.visibility = View.GONE
        vehicleModel = vehicleModels.get(position)
        type.text = vehicleModel.name
    }

    override val layoutResource: Int
        get() = R.layout.activity_wash
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var type:TextView
    lateinit var card_type:CardView
    lateinit var types:RecyclerView
    lateinit var difference:TextView
    lateinit var date:TextView
    lateinit var time:TextView
    lateinit var change:TextView
    lateinit var location:TextView

    lateinit var next:Button
    var address = ""
    var lat = ""
    var lng = ""

    lateinit var linearLayoutManager: LinearLayoutManager
     var vehicleModels = ArrayList<VehicleModel>()
    lateinit var listAdapter:ListsAdapter
    lateinit var vehicleModel: VehicleModel
    lateinit var myVehiclesModel: MyVehiclesModel


    override fun initializeComponents() {
        address = intent.getStringExtra("address")
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")

        myVehiclesModel = intent.getSerializableExtra("vehicle") as MyVehiclesModel
        Log.e("city"," "+lat+" "+lng)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        type = findViewById(R.id.type)
        card_type = findViewById(R.id.card_type)
        types = findViewById(R.id.types)
        difference = findViewById(R.id.difference)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)

        change = findViewById(R.id.change)
        location = findViewById(R.id.location)
        next = findViewById(R.id.next)
        title.text = getString(R.string.Vehicle_wash_request)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListsAdapter(mContext,vehicleModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = listAdapter
        location.text = address
        getTypes()
        type.setOnClickListener {
            if (card_type.visibility==View.GONE){
                card_type.visibility = View.VISIBLE
            }else{
                card_type.visibility = View.GONE
            }
        }
        difference.setOnClickListener { startActivity(Intent(this@WashActivity,WashTypeActivity::class.java)) }
        change.setOnClickListener { onBackPressed()
        finish()}
        date.setOnClickListener { startActivityForResult(Intent(this@WashActivity,DateActivity::class.java),1) }
        time.setOnClickListener { startActivityForResult(Intent(this@WashActivity,TimeActivity::class.java),2) }
        next.setOnClickListener {
            if (CommonUtil.checkTextError(type,getString(R.string.Determine_the_type_of_washing))||
                    CommonUtil.checkTextError(date,getString(R.string.Select_the_date))||
                    CommonUtil.checkTextError(time,getString(R.string.Determine_the_time))){
                return@setOnClickListener
            }else{
                if (address.equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.your_location))
                }else{
                    addorder()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            Log.e("onActivityResult", data?.getStringExtra("result"))
            date.text = data?.getStringExtra("result")
        }else if (resultCode == 2){
            Log.e("onActivityResult", data?.getStringExtra("time"))
            time.text = data?.getStringExtra("time")
        }
    }

    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getWash_types(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            card_type.visibility = View.GONE
                        }else{
                            listAdapter.updateAll(response.body()?.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun addorder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.addWashOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,myVehiclesModel.id!!,"wash",lat,lng,address,date.text.toString(),time.text.toString(),vehicleModel.id!!,1)?.enqueue(object :Callback<AddOrderResponse>{
            override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AddOrderResponse>,
                response: Response<AddOrderResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@WashActivity,WashInvoiceActivity::class.java)
                        intent.putExtra("order",response.body()?.data!!)
                        intent.putExtra("lat",lat)
                        intent.putExtra("lng",lng)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}