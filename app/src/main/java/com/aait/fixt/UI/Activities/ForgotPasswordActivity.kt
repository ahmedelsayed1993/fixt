package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.UserResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_forgot_passwrod
    lateinit var confirm:Button
    lateinit var phone: EditText
    lateinit var back: ImageView

    override fun initializeComponents() {
        confirm = findViewById(R.id.confirm)
        phone = findViewById(R.id.phone)
        back = findViewById(R.id.back)
        confirm.setOnClickListener { if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
            return@setOnClickListener
        }else{
            fogotPass()
        }}

    }
    fun fogotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.forgotPass(phone.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent =
                            Intent(this@ForgotPasswordActivity, NewPasswordActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}