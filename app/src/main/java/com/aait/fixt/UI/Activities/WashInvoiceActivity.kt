package com.aait.fixt.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Views.ListDialog
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.view.inputmethod.EditorInfo
import android.view.KeyEvent.KEYCODE_ENTER
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import com.aait.fixt.Models.*


class WashInvoiceActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        listDialog.dismiss()
        typeModel = types.get(position)
        wash_type.text = typeModel.name
        update()

    }

    override val layoutResource: Int
        get() = R.layout.activity_wash_invoice
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var time:TextView
    lateinit var location:TextView
    lateinit var wash_type:TextView
    lateinit var change:TextView
    lateinit var amount:TextView
    lateinit var code:EditText
    lateinit var by_card:RadioButton
    lateinit var cash:RadioButton
    lateinit var confirm:Button
    lateinit var credit:RadioButton
    lateinit var apple:RadioButton
    lateinit var listDialog: ListDialog
    lateinit var typeModel:VehicleModel
    lateinit var inviceModel: InviceModel
    var types = ArrayList<VehicleModel>()
    var pay =""
    var order = 0
    var lat = ""
    var lng = ""
    lateinit var msg:TextView

    override fun initializeComponents() {
        order = intent.getIntExtra("order",0)
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        time = findViewById(R.id.time)
        location = findViewById(R.id.location)
        wash_type = findViewById(R.id.wash_type)
        change = findViewById(R.id.change)
        amount = findViewById(R.id.amount)
        code = findViewById(R.id.code)
        by_card = findViewById(R.id.by_card)
        cash = findViewById(R.id.cash)
        confirm = findViewById(R.id.confirm)
        credit = findViewById(R.id.credit)
        apple = findViewById(R.id.apple)
        msg = findViewById(R.id.msg)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Order_invoice)
        by_card.setOnClickListener { pay = "online" }
        cash.setOnClickListener { pay = "cash" }
        change.setOnClickListener { getTypes() }
        getInvoice()

        confirm.setOnClickListener {
            if (pay.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.choose_payment))
            }else{
                updatepay()
            }
        }
        code.setOnEditorActionListener(object : OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (code.text.toString().equals("")){

                    }else {
                        code()
                    }
                }
                return false
            }
        })
    }

    fun setdata(inviceModel: InviceModel){
        time.text = inviceModel.time+"  -  "+inviceModel.date
        location.text = inviceModel.address
        wash_type.text = inviceModel.wash_type
        amount.text = inviceModel.total.toString()+""+getString(R.string.reyal)

    }
    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getWash_types(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                       types = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@WashInvoiceActivity,types,getString(R.string.Washing_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun getInvoice(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.invoice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,order)?.enqueue(object :Callback<InvoiceResponse>{
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>,
                response: Response<InvoiceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setdata(response.body()?.data!!)
                        inviceModel = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.washUpdate(mLanguagePrefManager.appLanguage,order,mSharedPrefManager.userData.id!!,lat,lng,inviceModel.address!!,null,typeModel.id!!,null,1)?.enqueue(object :Callback<InvoiceResponse>{
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>,
                response: Response<InvoiceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setdata(response.body()?.data!!)
                        inviceModel = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun updatepay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.washUpdate(mLanguagePrefManager.appLanguage,order,mSharedPrefManager.userData.id!!,lat,lng,inviceModel.address!!,pay,null,null,1)?.enqueue(object :Callback<InvoiceResponse>{
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>,
                response: Response<InvoiceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@WashInvoiceActivity,BackToHomeActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun code(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.discount(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,order,code.text.toString())?.enqueue(object :Callback<CodeResponse>{
            override fun onFailure(call: Call<CodeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CodeResponse>, response: Response<CodeResponse>) {
               hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        amount.text = response.body()?.data.toString()+""+getString(R.string.reyal)
                        msg.visibility = View.GONE
                    }else{
                        msg.visibility = View.VISIBLE
                        msg.text = response.body()?.msg!!
                    }
                }
            }

        })
    }
}