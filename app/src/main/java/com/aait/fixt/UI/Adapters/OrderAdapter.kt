package com.aait.fixt.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import com.aait.fixt.Base.ParentRecyclerAdapter
import com.aait.fixt.Base.ParentRecyclerViewHolder
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Models.OrderModel
import com.aait.fixt.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OrderAdapter (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
    ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val orderModel = data.get(position)
        viewHolder.order_num.text = orderModel.id.toString()
        viewHolder.service.text = orderModel.service
        viewHolder.price.text = orderModel.total+mcontext.getString(R.string.rs)
        viewHolder.time.text = orderModel.time
        viewHolder.date.text = orderModel.date



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var order_num=itemView.findViewById<TextView>(R.id.order_num)
        internal var service = itemView.findViewById<TextView>(R.id.service)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var date = itemView.findViewById<TextView>(R.id.date)
        internal var time = itemView.findViewById<TextView>(R.id.time)


    }
}