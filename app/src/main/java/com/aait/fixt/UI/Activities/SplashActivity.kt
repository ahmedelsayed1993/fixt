package com.aait.fixt.UI.Activities

import android.content.Intent
import android.os.Handler
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R
import me.ibrahimsn.lib.SmoothBottomBar

class SplashActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    private val SPLASH_DISPLAY_LENGTH = 5000

    override fun initializeComponents() {
        Handler().postDelayed(Runnable {
            if (mSharedPrefManager.loginStatus!!) {
                val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(mainIntent)
                finish()
            }else{
                val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(mainIntent)
                finish()
            }
        }, SPLASH_DISPLAY_LENGTH.toLong())

    }


}
