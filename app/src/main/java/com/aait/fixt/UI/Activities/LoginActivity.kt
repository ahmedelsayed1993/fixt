package com.aait.fixt.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.UserResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var login:Button
    lateinit var forgot:TextView
    lateinit var register:LinearLayout
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var lang:TextView
    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        login = findViewById(R.id.login)
        forgot = findViewById(R.id.forgot)
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        lang = findViewById(R.id.lang)
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            lang.text = "ع"
        }else{
            lang.text = "En"
        }
        lang.setOnClickListener {
            if (mLanguagePrefManager.appLanguage.equals("ar")){
                mLanguagePrefManager.appLanguage = "en"
                startActivity(Intent(this@LoginActivity,SplashActivity::class.java))
                this@LoginActivity.finish()
            }else{
                mLanguagePrefManager.appLanguage = "ar"
                startActivity(Intent(this@LoginActivity,SplashActivity::class.java))
                this@LoginActivity.finish()
            }
        }


        login.setOnClickListener {  if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkEditError(password,getString(R.string.password))){
            return@setOnClickListener
        }else{
            login()
        }}
        forgot.setOnClickListener { startActivity(Intent(this@LoginActivity,ForgotPasswordActivity::class.java)) }
        register.setOnClickListener { startActivity(Intent(this@LoginActivity,PreRegisterActivity::class.java))
            finish()}

    }

    fun login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.login(mLanguagePrefManager.appLanguage,phone.text.toString(),password.text.toString(),deviceID,"android")
            ?.enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                    Log.e("fff", Gson().toJson(t))

                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    Log.e("sss", Gson().toJson(response.body()?.data))
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            mSharedPrefManager.loginStatus = true
                            mSharedPrefManager.userData = response.body()?.data!!
                            startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                            finish()
                        }else if (response.body()?.value.equals("2")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            val intent =
                                Intent(this@LoginActivity, ActivateAccountActivity::class.java)
                            intent.putExtra("user", response.body()?.data)
                            startActivity(intent)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}