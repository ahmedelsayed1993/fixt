package com.aait.fixt.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.fixt.Base.ParentRecyclerAdapter
import com.aait.fixt.Base.ParentRecyclerViewHolder
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.R
import com.bumptech.glide.Glide


class MyVehicleAdapter (context: Context, data: MutableList<MyVehiclesModel>, layoutId: Int) :
    ParentRecyclerAdapter<MyVehiclesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    internal var selectedPosition = 0

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val myVehiclesModel = data.get(position)
        if (myVehiclesModel.plate_number.equals("")){
            holder.num.text = mcontext.getString(R.string.undefined)
        }else {
            holder.num!!.setText(myVehiclesModel.plate_number + myVehiclesModel.plate_letters)
        }
        holder.model!!.text = myVehiclesModel.vehicle
        Glide.with(mcontext).load(myVehiclesModel.image).into(holder.image)
        holder.itemView.setOnClickListener(View.OnClickListener {
                view ->
            onItemClickListener.onItemClick(view,position)
            })





    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {






        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var num = itemView.findViewById<TextView>(R.id.number)
        internal var model = itemView.findViewById<TextView>(R.id.model)

    }
}