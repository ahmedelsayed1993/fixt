package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.UserModel
import com.aait.fixt.Models.UserResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var user_name:TextView
    lateinit var phone:TextView
    lateinit var email:TextView
    lateinit var edit:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        edit = findViewById(R.id.edit)
        back.setOnClickListener { startActivity(Intent(this@ProfileActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.profile)
        getProfile()
        edit.setOnClickListener { startActivity(Intent(this@ProfileActivity,EditProfileActivity::class.java)) }

    }
    fun setData(userModel: UserModel){
        Glide.with(mContext).load(userModel.avatar).into(image)
        name.text = userModel.name
        user_name.text = userModel.name
        phone.text = userModel.phone
        email.text = userModel.email

    }

    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,null,null,null)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}