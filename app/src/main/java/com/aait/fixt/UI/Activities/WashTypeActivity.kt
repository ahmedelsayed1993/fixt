package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.AboutResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WashTypeActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var about_app: TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about_app = findViewById(R.id.about_app)
        back.setOnClickListener{onBackPressed()
            finish()}
        title.text = getString(R.string.Wash_types)
        getAbout()

    }
    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.washing(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutResponse> {
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutResponse>, response: Response<AboutResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about_app.text = response.body()?.data
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        about_app.text = getString(R.string.content_not_found_you_can_still_search_the_app_freely)
                    }
                }
            }

        })
    }
}