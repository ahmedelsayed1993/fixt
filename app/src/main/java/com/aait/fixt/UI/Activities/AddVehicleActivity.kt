package com.aait.fixt.UI.Activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.BaseResponse
import com.aait.fixt.Models.VehicleModel
import com.aait.fixt.Models.VehiclesResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Views.ListDialog
import com.aait.fixt.Uitls.CommonUtil
import com.aait.fixt.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class AddVehicleActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        listDialog.dismiss()
        if (selected == 0){
            vehicleModel = vehicleModels.get(position)
            vehicle_type.text = vehicleModel!!.name
            vehicle = vehicleModel!!.name.toString()
            vehicle_model.text = ""
        }else if (selected == 1){
            model = models.get(position)
            vehicle_model.text = model!!.name
            Model = model!!.name.toString()
        }else if (selected == 2){
            year = years.get(position)
            year_make.text = year!!.name
        }else if (selected == 3){
            engine = engines.get(position)
            engine_type.text = engine!!.name
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_add_vehicle
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var vehicle_type:TextView
    lateinit var vehicle_model:TextView
    lateinit var engine_type:TextView
    lateinit var year_make:TextView
    lateinit var listDialog: ListDialog
    lateinit var vehicleModels:ArrayList<VehicleModel>
     var vehicleModel: VehicleModel? = null
    lateinit var models:ArrayList<VehicleModel>
    lateinit var model: VehicleModel
    lateinit var years:ArrayList<VehicleModel>
    lateinit var year: VehicleModel
    lateinit var engines:ArrayList<VehicleModel>
    lateinit var engine: VehicleModel
    var selected = 0
    var vehicle = ""
    var Model = ""
    lateinit var add_image:TextView
    lateinit var click:TextView
    lateinit var add:Button
    private var ImageBasePath: String? = null
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        vehicle_type = findViewById(R.id.vehicle_type)
        vehicle_model = findViewById(R.id.vehicle_model)
        engine_type = findViewById(R.id.engine_type)
        year_make = findViewById(R.id.make_year)
        add_image = findViewById(R.id.add_image)
        click = findViewById(R.id.click)
        add = findViewById(R.id.add)
        years = ArrayList()
        back.setOnClickListener { startActivity(Intent(this@AddVehicleActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.add_new_vehicle)
        vehicle_type.setOnClickListener {
            selected =0
            getVehicle()  }

        vehicle_model.setOnClickListener {
            if (vehicle.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.choose_vehicle))
            }else{
                selected = 1
                getModels()
            }
        }
        year_make.setOnClickListener {
            selected = 2
            years.clear()
            for (i in Calendar.getInstance().get(Calendar.YEAR) downTo 2000) {
                years.add(VehicleModel(i, i.toString() + ""))
            }
            listDialog = ListDialog(
                mContext,
                this@AddVehicleActivity,
                years,
                getString(R.string.year_make)
            )
            listDialog.show()
        }
        engine_type.setOnClickListener {
            selected = 3
            if (Model.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.choose_model))
            }else{
                getEngines()
            }
        }

        click.setOnClickListener { startActivity(Intent(this@AddVehicleActivity,ContactUsActivity::class.java))
        finish()}

        add.setOnClickListener {
            if (CommonUtil.checkTextError(vehicle_type,getString(R.string.vehicle_type))||
                  CommonUtil.checkTextError(vehicle_model,getString(R.string.vehicle_model))||
                    CommonUtil.checkTextError(year_make,getString(R.string.year_make))||
                    CommonUtil.checkTextError(engine_type,getString(R.string.engine_type))){
                return@setOnClickListener
            }else{
                if (ImageBasePath!=null){addWithImage(ImageBasePath!!)

                }else{
                  addVehicle()
                }
            }
        }
        add_image.setOnClickListener { if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        } }

    }
    fun getVehicle(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getVehicles(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        vehicleModels = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@AddVehicleActivity,vehicleModels,getString(R.string.vehicle_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getModels(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getModels(mLanguagePrefManager.appLanguage,vehicleModel!!.id!!)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        models = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@AddVehicleActivity,models,getString(R.string.vehicle_model))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun getEngines(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getEngines(mLanguagePrefManager.appLanguage,model!!.id!!)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        engines = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@AddVehicleActivity,engines,getString(R.string.engine_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]
            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            add_image.text=ImageBasePath!!
            if (ImageBasePath != null) {


            }
        }
    }
    fun addVehicle(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.addVehicle(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,vehicleModel!!.id!!,model.id!!,engine.id!!,year.id!!)?.enqueue(object :Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@AddVehicleActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun addWithImage(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image_form", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.addVehiclewithImage(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,vehicleModel!!.id!!,model.id!!,engine.id!!,year.id!!,filePart)?.enqueue(object :Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@AddVehicleActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}