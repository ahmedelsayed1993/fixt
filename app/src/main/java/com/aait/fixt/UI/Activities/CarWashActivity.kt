package com.aait.fixt.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.GPS.GPSTracker
import com.aait.fixt.GPS.GpsTrakerListener
import com.aait.fixt.Models.AboutResponse
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import com.aait.fixt.Uitls.DialogUtil
import com.aait.fixt.Uitls.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class CarWashActivity : Parent_Activity(),
    OnMapReadyCallback,GoogleMap.OnMapClickListener,
    GpsTrakerListener {
    override fun onMapClick(p0: LatLng?) {
        Log.e("LatLng", p0.toString())
        mLang = p0?.latitude?.let { java.lang.Double.toString(it) }.toString()
        mLat = p0?.longitude?.let { java.lang.Double.toString(it) }.toString()
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker(p0?.latitude, p0?.longitude)
            mLat = p0?.latitude.toString()
            mLang = p0?.longitude.toString()
            val addresses: List<Address>

            geocoder = Geocoder(this@CarWashActivity, Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                    java.lang.Double.parseDouble(mLat),
                    java.lang.Double.parseDouble(mLang),
                    1
                )

                if (addresses.isEmpty()) {
                    Toast.makeText(
                        this@CarWashActivity,
                        resources.getString(R.string.detect_location),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    your_location.text = addresses[0].getAddressLine(0)
                    // CommonUtil.makeToast(mContext,mResult);
                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker(p0?.latitude, p0?.longitude)
        }

    }

    lateinit var map: MapView
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var city = ""
    lateinit var myVehiclesModel:MyVehiclesModel
    internal lateinit var markerOptions: MarkerOptions
    private var mAlertDialog: AlertDialog? = null

    lateinit var your_location: TextView
    lateinit var back: ImageView
    lateinit var confirm: Button


    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
       googleMap.setOnMapClickListener(this)
        getLocationWithPermission()
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_location

    override fun initializeComponents() {
        myVehiclesModel = intent.getSerializableExtra("vehicle") as MyVehiclesModel
        map = findViewById(R.id.map)
        your_location = findViewById(R.id.your_location)
        back = findViewById(R.id.back)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { startActivity(Intent(this@CarWashActivity,MainActivity::class.java))
            finish()}
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        confirm.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Avaailable(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
                ,"wash",mLat,mLang)?.enqueue(object : Callback<AboutResponse> {
                override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutResponse>,
                    response: Response<AboutResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@CarWashActivity,WashActivity::class.java)
                            intent.putExtra("address",your_location.text)
                            intent.putExtra("lat",mLat)
                            intent.putExtra("lng",mLang)

                            intent.putExtra("vehicle",myVehiclesModel)
                            startActivity(intent)
                        }else if (response.body()?.value.equals("0")){
                            //Toast.makeText(mContext,response.body()?.msg!!,Toast.LENGTH_LONG).show()
                            CommonUtil.makeToastTop(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })



        }

    }


    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.washlocation))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        your_location.text = addresses[0].getAddressLine(0)


                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }



//    override fun onMapClick(latLng: LatLng) {
//        Log.e("LatLng", latLng.toString())
//        mLang = java.lang.Double.toString(latLng.latitude)
//        mLat = java.lang.Double.toString(latLng.longitude)
//        if (myMarker != null) {
//            myMarker.remove()
//            putMapMarker(latLng.latitude, latLng.longitude)
//        } else {
//            putMapMarker(latLng.latitude, latLng.longitude)
//        }
//
//        if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
//            putMapMarker(latLng.latitude, latLng.longitude)
//            mLat = latLng.latitude.toString()
//            mLang = latLng.longitude.toString()
//            val addresses: List<Address>
//
//            geocoder = Geocoder(this@CarWashActivity, Locale.getDefault())
//
//            try {
//                addresses = geocoder.getFromLocation(
//                    java.lang.Double.parseDouble(mLat),
//                    java.lang.Double.parseDouble(mLang),
//                    1
//                )
//
//                if (addresses.isEmpty()) {
//                    Toast.makeText(
//                        this@CarWashActivity,
//                        resources.getString(R.string.detect_location),
//                        Toast.LENGTH_SHORT
//                    ).show()
//                } else {
//                    your_location.text = addresses[0].getAddressLine(0)
//                    // CommonUtil.makeToast(mContext,mResult);
//                }
//
//
//            } catch (e: IOException) {
//            }
//
//            googleMap.clear()
//            putMapMarker(latLng.latitude, latLng.longitude)
//        }
//
//    }
}