package com.aait.fixt.UI.Activities

import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.CarDetails
import com.aait.fixt.Models.CarModel
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VechicleDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_vehicle_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView
    lateinit var type:TextView
    lateinit var make_year:TextView
    lateinit var model:TextView
    lateinit var capacity:TextView
    lateinit var oil_kilo:TextView
    lateinit var oil_date:TextView
    lateinit var oil_filter_kilo:TextView
    lateinit var oil_filter_date:TextView
    lateinit var makena_kilo:TextView
    lateinit var makena_date:TextView
    lateinit var air_kilo:TextView
    lateinit var air_date:TextView
    lateinit var oil_type:TextView
    lateinit var viscosity:TextView
    lateinit var oil:TextView
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        type = findViewById(R.id.type)
        make_year = findViewById(R.id.make_year)
        model = findViewById(R.id.model)
        capacity = findViewById(R.id.capacity)
        oil_kilo = findViewById(R.id.oil_kilo)
        oil_date = findViewById(R.id.oil_date)
        oil_filter_kilo = findViewById(R.id.oil_filter_kilo)
        oil_filter_date = findViewById(R.id.oil_filter_date)
        makena_kilo = findViewById(R.id.makena_kilo)
        makena_date = findViewById(R.id.makena_date)
        air_kilo = findViewById(R.id.air_kilo)
        air_date = findViewById(R.id.air_date)
        oil_type = findViewById(R.id.oil_type)
        viscosity = findViewById(R.id.viscosity)
        oil = findViewById(R.id.oil)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.car_data)
        getCar()

    }
    fun getCar(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(
            object : Callback<CarDetails>{
                override fun onFailure(call: Call<CarDetails>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(call: Call<CarDetails>, response: Response<CarDetails>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            setData(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
    fun setData(carModel:CarModel){
        Glide.with(mContext).load(carModel.image).into(image)
        if (carModel.vehicle.equals("")){
            type.text = getString(R.string.undefined)
        }else {
            type.text = carModel.vehicle
        }
        if (carModel.viscosity.equals("")){
            viscosity.text = getString(R.string.undefined)
        }else {
            viscosity.text = carModel.viscosity
        }
        if (carModel.oil.equals("")){
            oil.text = getString(R.string.undefined)
        }else {
            oil.text = carModel.oil
        }
        if (carModel.model.equals("")){
            model.text =getString(R.string.undefined)
        }else {
            model.text = carModel.model
        }
        if (carModel.engine.equals("")){
            capacity.text = getString(R.string.undefined)
        }else {
            capacity.text = carModel.engine
        }
        if (carModel.year.equals("")){
            make_year.text = getString(R.string.undefined)
        }else {
            make_year.text = carModel.year
        }
        if (carModel.oil_type.equals("")){
            oil_type.text = getString(R.string.undefined)
        }else {
            oil_type.text = carModel.oil_type
        }
        if (carModel.date_oil_filter.equals("")){
            oil_filter_date.text = getString(R.string.oil_change_data)+" : "+getString(R.string.undefined)
        }else {
            oil_filter_date.text =
                getString(R.string.oil_change_data) + " : " + carModel.date_oil_filter
        }
        oil_filter_kilo.text = getString(R.string.km_num)+" : "+carModel.change_km_oil
        if (carModel.oil_change_date.equals("")){
            oil_date.text = getString(R.string.oil_change_data)+" : "+getString(R.string.undefined)
        }else {
            oil_date.text = getString(R.string.oil_change_data) + " : " + carModel.oil_change_date
        }
        oil_kilo.text = getString(R.string.km_num)+" : "+carModel.oil_change_km
        if (carModel.date_filter_makina.equals("")){
            makena_date.text = getString(R.string.oil_change_data)+" : "+getString(R.string.undefined)
        }else {
            makena_date.text =
                getString(R.string.oil_change_data) + " : " + carModel.date_filter_makina
        }
        makena_kilo.text = getString(R.string.km_num)+" : "+carModel.makina_change_km
        if (carModel.date_filter_air.equals("")){
            air_date.text = getString(R.string.oil_change_data)+" : "+getString(R.string.undefined)
        }else {
            air_date.text = getString(R.string.oil_change_data) + " : " + carModel.date_filter_air
        }
        air_kilo.text = getString(R.string.km_num)+" : "+carModel.air_change_km
    }
}