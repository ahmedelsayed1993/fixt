package com.aait.fixt.UI.Activities

import android.widget.Button
import android.widget.CalendarView
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R
import java.util.*
import android.app.Activity
import android.content.Intent




class DateActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = com.aait.fixt.R.layout.activity_date
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var calender:CalendarView
    lateinit var confirm:Button
    lateinit var date:TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        calender = findViewById(R.id.calender)
        confirm = findViewById(R.id.confirm)
        date = findViewById(R.id.date)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Select_the_date)
        calender.minDate = Calendar.getInstance().timeInMillis
        calender?.setOnDateChangeListener { view, year, month, dayOfMonth ->
            // Note that months are indexed from 0. So, 0 means January, 1 means february, 2 means march etc.
            date.text = ""+ dayOfMonth + "/" + (month + 1) + "/" + year

        }
        confirm.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra("result", date.text)
            setResult(1, returnIntent)
            finish()
        }


    }
}