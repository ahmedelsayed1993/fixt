package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R
import com.aait.fixt.UI.Adapters.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class OrdersActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_orders
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this@OrdersActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.my_order)
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)

    }
}