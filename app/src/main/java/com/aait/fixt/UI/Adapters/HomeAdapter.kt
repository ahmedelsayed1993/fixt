package com.aait.fixt.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import com.aait.fixt.Base.ParentRecyclerAdapter
import com.aait.fixt.Base.ParentRecyclerViewHolder
import com.aait.fixt.Models.ListModel
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class HomeAdapter (context: Context, data: MutableList<MyVehiclesModel>, layoutId: Int) :
    ParentRecyclerAdapter<MyVehiclesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    internal var selectedPosition = 0

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val myVehiclesModel = data.get(position)
        viewHolder.num!!.setText(myVehiclesModel.vehicle)
        if (myVehiclesModel.plate_number.equals("")){
            viewHolder.plate_num!!.text = mcontext.getString(R.string.undefined)
        }else {
            viewHolder.plate_num!!.text =
                myVehiclesModel.plate_letters + myVehiclesModel.plate_number
        }
        if (myVehiclesModel.date_change.equals("")){
            viewHolder.date.text = mcontext.getString(R.string.undefined)
        }else {
            viewHolder.date.text = myVehiclesModel.date_change
        }
        viewHolder.distance.text = myVehiclesModel.km_number+mcontext.getString(R.string.km)
        viewHolder.check.setChecked(selectedPosition == position)
        viewHolder.check.setTag(position)
        Glide.with(mcontext).load(myVehiclesModel.image).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener {
                view ->
            onItemClickListener.onItemClick(view,position)
            selectedPosition = viewHolder.check.getTag() as Int
            notifyDataSetChanged()})
        viewHolder.check.setOnClickListener(View.OnClickListener {
                view ->
            onItemClickListener.onItemClick(view,position)
            selectedPosition = viewHolder.check.getTag() as Int
            notifyDataSetChanged()})



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var check=itemView.findViewById<RadioButton>(R.id.check)
        internal var image = itemView.findViewById<CircleImageView>(R.id.image)
        internal var num = itemView.findViewById<TextView>(R.id.num)
        internal var plate_num = itemView.findViewById<TextView>(R.id.plate_num)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)
        internal var date = itemView.findViewById<TextView>(R.id.date)

    }
}