package com.aait.fixt.UI.Activities

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.*
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Views.FilterDialog
import com.aait.fixt.UI.Views.ListDialog
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OilInvoiceActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (selected==0){
            listDialog.dismiss()
            oilModel = oils.get(position)
            oil.text = oilModel.name
            update(oilModel.id,null,null,null,null,null,null)
        }else if (selected ==1){
            listDialog.dismiss()
            typeModel = types.get(position)
            type.text = typeModel.name
            id = typeModel.id!!
            update(typeModel.id)

        }else if (selected == 2){
            listDialog.dismiss()
            viscosotyModel = viscosities.get(position)
            viscosity.text = viscosotyModel.name
            update(null,null,viscosotyModel.id,null,null,null,null)
        }else if (selected == 3){
            filterDialog.dismiss()
            oilfilter = oFilters.get(position)
            oil_ = oilfilter.id
            oil_filter.text = oilfilter.name
            update(null,null,null,oilfilter.id,null,null,null)
        }
        else if (selected == 4){
            filterDialog.dismiss()
            makinafilter = VFilters.get(position)
            makena = makinafilter.id
            Makina_filter.text = makinafilter.name
            update(null,null,null,null,makinafilter.id,null,null)
        }
        else if (selected == 5){
            filterDialog.dismiss()
            airfilter = AFilter.get(position)
            air = airfilter.id
            air_filter.text = airfilter.name
            update(null,null,null,null,null,airfilter.id,null)
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_oil_invoice
    lateinit var back: ImageView
    lateinit var title: TextView
    var address = ""
    var lat = ""
    var lng = ""
    lateinit var can_price:TextView
    lateinit var oil_filter_price:TextView
    lateinit var malena_price:TextView
    lateinit var air_price:TextView
    lateinit var air_lay: LinearLayout
    lateinit var makena_lay: LinearLayout
    lateinit var oil_lay: LinearLayout
    lateinit var oil_filter_change: TextView
    lateinit var air_change: TextView
    lateinit var makena_change: TextView
    lateinit var myVehiclesModel: MyVehiclesModel
    lateinit var oil: TextView
    lateinit var oil_change: TextView
    lateinit var can_num: EditText
    lateinit var type: TextView
    lateinit var type_change: TextView
    lateinit var viscosity: TextView
    lateinit var viscosity_change: TextView
    lateinit var oil_filter: TextView
    lateinit var oil_delete: TextView
    lateinit var Makina_filter: TextView
    lateinit var makina_delete: TextView
    lateinit var air_filter: TextView
    lateinit var air_delete: TextView
    lateinit var next: Button
    lateinit var listDialog: ListDialog
    lateinit var filterDialog: FilterDialog
    var oils = ArrayList<VehicleModel>()
    var types = ArrayList<VehicleModel>()
    var viscosities = ArrayList<VehicleModel>()
    lateinit var oilModel: VehicleModel
    lateinit var typeModel: VehicleModel
    lateinit var viscosotyModel: VehicleModel
    lateinit var oilfilter: FilterModel
    lateinit var makinafilter: FilterModel
    lateinit var airfilter: FilterModel
    var oFilters = ArrayList<FilterModel>()
    var VFilters  = ArrayList<FilterModel>()
    var AFilter = ArrayList<FilterModel>()
    var selected = 0
    lateinit var oil_purpose: CheckBox
    var air:String?=null
    var oil_:String?=null
    var makena:String?=null
    var purpose = 0
    lateinit var lay: LinearLayout
    lateinit var amount:TextView
    lateinit var code:EditText
    lateinit var by_card:RadioButton
    lateinit var cash:RadioButton
    lateinit var confirm:Button
    lateinit var credit:RadioButton
    lateinit var apple:RadioButton
    lateinit var time:TextView
    lateinit var location:TextView
    lateinit var msg:TextView
    lateinit var can_change:TextView
    var order = 0
    var pay =""
    var id = 0
    lateinit var inviceModel: InviceModel
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun initializeComponents() {
        order = intent.getIntExtra("order",0)
        oFilters.clear()
        VFilters.clear()
        AFilter.clear()

        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")

        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        can_price = findViewById(R.id.can_price)
        oil_filter_price = findViewById(R.id.oil_filter_price)
        air_price = findViewById(R.id.air_price)
        malena_price = findViewById(R.id.malena_price)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Order_invoice)
        msg = findViewById(R.id.msg)
        time = findViewById(R.id.time)
        location = findViewById(R.id.location)
        amount = findViewById(R.id.amount)
        code = findViewById(R.id.code)
        by_card = findViewById(R.id.by_card)
        cash = findViewById(R.id.cash)
        confirm = findViewById(R.id.confirm)
        credit = findViewById(R.id.credit)
        apple = findViewById(R.id.apple)
        oil = findViewById(R.id.oil)
        oil_change = findViewById(R.id.oil_change)
        can_num = findViewById(R.id.can_num)
        type = findViewById(R.id.type)
        type_change = findViewById(R.id.type_change)
        viscosity = findViewById(R.id.viscosity)
        oil_purpose = findViewById(R.id.oil_purposes)
        lay = findViewById(R.id.lay)
        viscosity_change = findViewById(R.id.viscosity_change)
        oil_filter = findViewById(R.id.oil_filter)
        oil_delete = findViewById(R.id.oil_delete)
        Makina_filter = findViewById(R.id.Makina_filter)
        makina_delete = findViewById(R.id.makina_delete)
        air_filter = findViewById(R.id.air_filter)
        air_delete = findViewById(R.id.air_delete)
        oil_filter_change = findViewById(R.id.oil_filter_change)
        air_change = findViewById(R.id.air_change)
        makena_change = findViewById(R.id.makina_change)
        air_lay = findViewById(R.id.air_lay)
        makena_lay = findViewById(R.id.makena_lay)
        oil_lay = findViewById(R.id.oil_lay)
        can_change = findViewById(R.id.cans_change)
        oFilters.add(FilterModel("original",getString(R.string.original)))
        oFilters.add(FilterModel("agency",getString(R.string.agency)))
        VFilters.add(FilterModel("original",getString(R.string.original)))
        VFilters.add(FilterModel("agency",getString(R.string.agency)))
        AFilter.add(FilterModel("original",getString(R.string.original)))
        AFilter.add(FilterModel("agency",getString(R.string.agency)))
        oil_purpose.setOnClickListener {
            if (oil_purpose.isChecked) {

                lay.foreground =
                    ColorDrawable(ContextCompat.getColor(mContext, R.color.transparent))
                purpose = 1
                update(null,null,null,null,null,null,purpose)
            } else {
                lay.foreground = null
                purpose = 0
                update(null,null,null,null,null,null,purpose)
            }
        }
        can_change.setOnClickListener {
            can_num.requestFocus()
            val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(can_num, InputMethodManager.SHOW_IMPLICIT)
        }

//        oil_filter.text = oFilters.get(0).name
//        oil_ = oFilters.get(0).id
//        Makina_filter.text = VFilters.get(0).name
//        makena = VFilters.get(0).id
//        air_filter.text = AFilter.get(0).name
//        air = AFilter.get(0).id
//        oilfilter = oFilters.get(0)
//        makinafilter = VFilters.get(0)
//        airfilter = AFilter.get(0)
        oil_filter_change.setOnClickListener { selected = 3
            filterDialog = FilterDialog(mContext,this,oFilters,getString(R.string.Oil_filter))
            filterDialog.show()
        }
        makena_change.setOnClickListener { selected = 4
            filterDialog = FilterDialog(mContext,this,VFilters,getString(R.string.Makina_filter))
            filterDialog.show()
        }
        air_change.setOnClickListener { selected = 5
            filterDialog = FilterDialog(mContext,this,AFilter,getString(R.string.Air_condition_filter))
            filterDialog.show()
        }
        oil_delete.setOnClickListener { oil_lay.visibility =View.GONE
            oil_ = "deleted"
            update(null,null,null,"deleted",null,null,null)}
        air_lay.setOnClickListener { air_lay.visibility = View.GONE
            air = "deleted"
            update(null,null,null,null,null,"deleted",null)}
        makina_delete.setOnClickListener { makena_lay.visibility = View.GONE
            makena = "deleted"
            update(null,null,null,null,"deleted",null,null)}
        oil_change.setOnClickListener {
            selected = 0
            getOil()

        }
        getInvoice()

        type_change.setOnClickListener {
            selected = 1
            getTypes()

        }
        viscosity_change.setOnClickListener {
            selected = 2
            if (id == 0){

            }else {
                getViscosity(id)
            }

        }
        by_card.setOnClickListener { pay = "online" }
        cash.setOnClickListener { pay = "cash" }

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(can_num,getString(R.string.Number_of_cans))){
                return@setOnClickListener
            }else {
                if (pay.equals("")) {
                    CommonUtil.makeToast(mContext, getString(R.string.choose_payment))
                } else {
                    updatepay()
                }
            }
        }

        code.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                   if (code.text.toString().equals("")){

                   }else {
                       code()
                   }
                }
                return false
            }
        })
    }
    fun getOil(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOils(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        oils = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@OilInvoiceActivity,oils,getString(R.string.oil))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getTypes(){

        Client.getClient()?.create(Service::class.java)?.getOilTypes(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {

                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        types = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@OilInvoiceActivity,types,getString(R.string.Oil_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getViscosity(id:Int){

        Client.getClient()?.create(Service::class.java)?.getViscosity(mLanguagePrefManager.appLanguage,id)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {

                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        viscosities = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@OilInvoiceActivity,viscosities,getString(R.string.Degree_of_viscosity))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun setdata(inviceModel: InviceModel){
        time.text = inviceModel.time+"  -  "+inviceModel.date
        location.text = inviceModel.address

        if (inviceModel.have_oil==0){
            lay.foreground = null
            purpose = 0
            oil_purpose.isChecked = false
            can_num.setText(inviceModel.number_cans.toString())
            can_price.text = inviceModel.price_cans.toString()+""+getString(R.string.reyal)
            oil.text = inviceModel.oil
            type.text = inviceModel.oil_type
            typeModel = VehicleModel(inviceModel.oil_type_id!!.toInt(),inviceModel.oil_type)
            viscosity.text = inviceModel.viscosity
            if (inviceModel.oil_filter.equals("")){
                oil_lay.visibility = View.GONE
            }else{
                oil_lay.visibility = View.VISIBLE
                if(inviceModel.oil_filter.equals("original")) {
                    oil_filter.text = getString(R.string.original)
                }else{
                    oil_filter.text = getString(R.string.agency)
                }
                oil_filter_price.text = inviceModel.price_oil_filter.toString()+getString(R.string.reyal)
            }
            if (inviceModel.filter_makina.equals("")){
                makena_lay.visibility = View.GONE
            }else{
                makena_lay.visibility = View.VISIBLE
                if(inviceModel.filter_makina.equals("original")) {
                    Makina_filter.text = getString(R.string.original)
                }else{
                    Makina_filter.text = getString(R.string.agency)
                }
                malena_price.text = inviceModel.price_filter_makina.toString()+getString(R.string.reyal)
            }
            if (inviceModel.filter_air.equals("")){
                air_lay.visibility = View.GONE
            }else{
                air_lay.visibility = View.VISIBLE
                if(inviceModel.filter_air.equals("original")) {
                    air_filter.text = getString(R.string.original)
                }else{
                    air_filter.text = getString(R.string.agency)
                }
                air_price.text = inviceModel.price_filter_air.toString()+getString(R.string.reyal)
            }
        }else{
            lay.foreground =
                ColorDrawable(ContextCompat.getColor(mContext, R.color.transparent))
            purpose = 1
            oil_purpose.isChecked = true
        }

        amount.text = inviceModel.total.toString()+""+getString(R.string.reyal)

    }

    fun getInvoice(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.invoice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,order)?.enqueue(object :Callback<InvoiceResponse>{
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onResponse(
                call: Call<InvoiceResponse>,
                response: Response<InvoiceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setdata(response.body()?.data!!)
                        inviceModel = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun update(oil:Int?,type:Int?,vis:Int?,oil_filter:String?,m_filter:String?,air_filter:String?,have:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OilUpdate(mLanguagePrefManager.appLanguage,order,mSharedPrefManager.userData.id!!
        ,lat,lng,inviceModel.address!!,null,air_filter,m_filter,oil_filter,can_num.text.toString(),type,have,vis,oil,null)
            ?.enqueue(object :Callback<InvoiceResponse>{
                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                @RequiresApi(Build.VERSION_CODES.M)
                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            setdata(response.body()?.data!!)
                            inviceModel = response.body()?.data!!
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    fun update(type:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OilUpdate(mLanguagePrefManager.appLanguage,order,mSharedPrefManager.userData.id!!
            ,lat,lng,inviceModel.address!!,null,null,null,null,can_num.text.toString(),type,null,null,null,null)
            ?.enqueue(object :Callback<InvoiceResponse>{
                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                @RequiresApi(Build.VERSION_CODES.M)
                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            setdata(response.body()?.data!!)
                            inviceModel = response.body()?.data!!
                            selected = 2
                            getViscosity(type!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    fun updatepay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OilUpdate(mLanguagePrefManager.appLanguage,order,mSharedPrefManager.userData.id!!,lat,lng,inviceModel.address!!,pay,null,null,null,can_num.text.toString(),null,null,null,null,null)?.enqueue(object :Callback<InvoiceResponse>{
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>,
                response: Response<InvoiceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@OilInvoiceActivity,BackToHomeActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun code(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.discount(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,order,code.text.toString())?.enqueue(object :Callback<CodeResponse>{
            override fun onFailure(call: Call<CodeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CodeResponse>, response: Response<CodeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        amount.text = response.body()?.data.toString()+""+getString(R.string.reyal)
                        msg.visibility = View.GONE
                    }else{
                        msg.visibility = View.VISIBLE
                        msg.text = response.body()?.msg!!
                    }
                }
            }

        })
    }

}