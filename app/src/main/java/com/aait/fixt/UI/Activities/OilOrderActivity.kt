package com.aait.fixt.UI.Activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.FilterModel
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Models.VehicleModel
import com.aait.fixt.Models.VehiclesResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.R.color.transparent
import com.aait.fixt.UI.Views.FilterDialog
import com.aait.fixt.UI.Views.ListDialog
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OilOrderActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {

        if (selected==0){
            listDialog.dismiss()
            oilModel = oils.get(position)
            oil.text = oilModel.name
        }else if (selected ==1){
            listDialog.dismiss()
            typeModel = types.get(position)
            type.text = typeModel.name
            id = typeModel.id!!
            viscosity.text = ""
        }else if (selected == 2){
            listDialog.dismiss()
            viscosotyModel = viscosities.get(position)
            viscosity.text = viscosotyModel.name
        }else if (selected == 3){
            filterDialog.dismiss()
            oilfilter = oFilters.get(position)
            oil_ = oilfilter.id
            oil_filter.text = oilfilter.name
        }
        else if (selected == 4){
            filterDialog.dismiss()
            makinafilter = VFilters.get(position)
            makena = makinafilter.id
            Makina_filter.text = makinafilter.name
        }
        else if (selected == 5){
            filterDialog.dismiss()
            airfilter = AFilter.get(position)
            air = airfilter.id
            air_filter.text = airfilter.name
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_order_oil
    lateinit var back:ImageView
    lateinit var title:TextView
    var address = ""
    var lat = ""
    var lng = ""
    lateinit var air_lay:LinearLayout
    lateinit var makena_lay:LinearLayout
    lateinit var oil_lay:LinearLayout
    lateinit var oil_filter_change:TextView
    lateinit var air_change:TextView
    lateinit var makena_change:TextView
    lateinit var myVehiclesModel: MyVehiclesModel
    lateinit var oil:TextView
    lateinit var oil_change:TextView
    lateinit var can_num:EditText
    lateinit var type:TextView
    lateinit var type_change:TextView
    lateinit var viscosity:TextView
    lateinit var viscosity_change:TextView
    lateinit var oil_filter:TextView
    lateinit var oil_delete:TextView
    lateinit var Makina_filter:TextView
    lateinit var makina_delete:TextView
    lateinit var air_filter:TextView
    lateinit var air_delete:TextView
    lateinit var next:Button
    lateinit var listDialog: ListDialog
    lateinit var filterDialog: FilterDialog
    var oils = ArrayList<VehicleModel>()
    var types = ArrayList<VehicleModel>()
    var viscosities = ArrayList<VehicleModel>()
    lateinit var oilModel:VehicleModel
    lateinit var typeModel:VehicleModel
    lateinit var viscosotyModel:VehicleModel
    lateinit var oilfilter:FilterModel
    lateinit var makinafilter:FilterModel
    lateinit var airfilter:FilterModel
    var oFilters = ArrayList<FilterModel>()
    var VFilters  = ArrayList<FilterModel>()
    var AFilter = ArrayList<FilterModel>()
    var selected = 0
    lateinit var oil_purpose:CheckBox
    lateinit var can_change:TextView
     var air:String?=null
    var oil_:String?=null
    var makena:String?=null
    var id = 0
    var purpose = 0
    lateinit var lay:LinearLayout
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    @TargetApi(Build.VERSION_CODES.O)
    @SuppressLint("Range")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun initializeComponents() {
        oFilters.clear()
        VFilters.clear()
        AFilter.clear()
        address = intent.getStringExtra("address")
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        can_change = findViewById(R.id.cans_change)
        myVehiclesModel = intent.getSerializableExtra("vehicle") as MyVehiclesModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Request_to_change_oil)
        oil = findViewById(R.id.oil)
        oil_change = findViewById(R.id.oil_change)
        can_num = findViewById(R.id.can_num)
        type = findViewById(R.id.type)
        type_change = findViewById(R.id.type_change)
        viscosity = findViewById(R.id.viscosity)
        oil_purpose = findViewById(R.id.oil_purposes)
        lay = findViewById(R.id.lay)
        viscosity_change = findViewById(R.id.viscosity_change)
        oil_filter = findViewById(R.id.oil_filter)
        oil_delete = findViewById(R.id.oil_delete)
        Makina_filter = findViewById(R.id.Makina_filter)
        makina_delete = findViewById(R.id.makina_delete)
        air_filter = findViewById(R.id.air_filter)
        air_delete = findViewById(R.id.air_delete)
        oil_filter_change = findViewById(R.id.oil_filter_change)
        air_change = findViewById(R.id.air_change)
        makena_change = findViewById(R.id.makina_change)
        air_lay = findViewById(R.id.air_lay)
        makena_lay = findViewById(R.id.makena_lay)
        oil_lay = findViewById(R.id.oil_lay)
        next = findViewById(R.id.next)
        oFilters.add(FilterModel("original",getString(R.string.original)))
        oFilters.add(FilterModel("agency",getString(R.string.agency)))
        VFilters.add(FilterModel("original",getString(R.string.original)))
        VFilters.add(FilterModel("agency",getString(R.string.agency)))
        AFilter.add(FilterModel("original",getString(R.string.original)))
        AFilter.add(FilterModel("agency",getString(R.string.agency)))
        oil_purpose.setOnClickListener {
            if (oil_purpose.isChecked) {

                lay.foreground =
                    ColorDrawable(ContextCompat.getColor(mContext, R.color.transparent))
                purpose = 1
            } else {
                lay.foreground = null
                purpose = 0
            }
        }
        getOil()
        getTypes()
        if (id ==0){

        }else {
            getViscosity(id)
        }

        can_change.setOnClickListener { can_num.requestFocus()
            val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(can_num, InputMethodManager.SHOW_IMPLICIT)}
        oil_filter.text = oFilters.get(0).name
        oil_ = oFilters.get(0).id
        Makina_filter.text = VFilters.get(0).name
        makena = VFilters.get(0).id
        air_filter.text = AFilter.get(0).name
        air = AFilter.get(0).id
        oilfilter = oFilters.get(0)
        makinafilter = VFilters.get(0)
        airfilter = AFilter.get(0)
        oil_filter_change.setOnClickListener { selected = 3
        filterDialog = FilterDialog(mContext,this,oFilters,getString(R.string.Oil_filter))
            filterDialog.show()
        }
        makena_change.setOnClickListener { selected = 4
            filterDialog = FilterDialog(mContext,this,VFilters,getString(R.string.Makina_filter))
            filterDialog.show()
        }
        air_change.setOnClickListener { selected = 5
            filterDialog = FilterDialog(mContext,this,AFilter,getString(R.string.Air_condition_filter))
            filterDialog.show()
        }
        oil_delete.setOnClickListener { oil_lay.visibility =View.GONE
        oil_ = null}
        air_lay.setOnClickListener { air_lay.visibility = View.GONE
        air = null}
        makina_delete.setOnClickListener { makena_lay.visibility = View.GONE
        makena = null}
        oil_change.setOnClickListener {
            selected = 0
            listDialog = ListDialog(mContext,this,oils,getString(R.string.oil))
            listDialog.show()

        }

        type_change.setOnClickListener {
            selected = 1
            listDialog = ListDialog(mContext,this,types,getString(R.string.Oil_type))
            listDialog.show()

        }
        viscosity_change.setOnClickListener {
            selected = 2
            if (id == 0){
                CommonUtil.makeToast(mContext,getString(R.string.Oil_type))
            }else{
                getViscosity(id)
            }


        }
        next.setOnClickListener {
            if (CommonUtil.checkTextError(oil,getString(R.string.oil))||
                    CommonUtil.checkEditError(can_num,getString(R.string.Number_of_cans))||
                    CommonUtil.checkTextError(type,getString(R.string.Oil_type))||
                    CommonUtil.checkTextError(viscosity,getString(R.string.Degree_of_viscosity))){
                return@setOnClickListener
            }else {

                val intent = Intent(this@OilOrderActivity, CompleteOilOrderActivity::class.java)
                intent.putExtra("have_oil", purpose)
                intent.putExtra("oil_id", oilModel.id)
                intent.putExtra("type", typeModel.id)
                intent.putExtra("num", can_num.text.toString())
                intent.putExtra("viscosity", viscosotyModel.id)
                intent.putExtra("oil", oil_)
                intent.putExtra("makena", makena)
                intent.putExtra("air", air)
                intent.putExtra("address", address)
                intent.putExtra("lat", lat)
                intent.putExtra("lng", lng)
                intent.putExtra("vehicle", myVehiclesModel)
                startActivity(intent)
            }

        }

    }
    fun getOil(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOils(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        oils = response.body()?.data!!
                        oilModel = response.body()?.data!!.get(0)
                        oil.text = response.body()?.data!!.get(0).name
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getTypes(){

        Client.getClient()?.create(Service::class.java)?.getOilTypes(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {

                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        types = response.body()?.data!!
                        typeModel = response.body()?.data!!.get(0)
                        type.text = response.body()?.data!!.get(0).name
                        id = types.get(0).id!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getViscosity(id:Int){

        Client.getClient()?.create(Service::class.java)?.getViscosity(mLanguagePrefManager.appLanguage,id)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {

                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        viscosities = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@OilOrderActivity,viscosities,getString(R.string.Degree_of_viscosity))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}