package com.aait.fixt.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.AddOrderResponse
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompleteOilOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_complete_oil_order
    lateinit var back: ImageView
    lateinit var title: TextView
    var address = ""
    var lat = ""
    var lng = ""
    lateinit var myVehiclesModel: MyVehiclesModel
    lateinit var date:TextView
    lateinit var time:TextView

    lateinit var change:TextView
    lateinit var location:TextView
    lateinit var next: Button
    var have_oil = 0
    var oil_id = 0
    var type = 0
    var num = ""
    var viscosity = 0
    var oil:String?=null
    var makena:String?=null
    var air:String?=null




    override fun initializeComponents() {
        address = intent.getStringExtra("address")
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        have_oil = intent.getIntExtra("have_oil",0)
        oil_id = intent.getIntExtra("oil_id",0)
        type = intent.getIntExtra("type",0)
        num = intent.getStringExtra("num")
        viscosity = intent.getIntExtra("viscosity",0)
        oil = intent.getStringExtra("oil")
        makena = intent.getStringExtra("makena")
        air = intent.getStringExtra("air")
        myVehiclesModel = intent.getSerializableExtra("vehicle") as MyVehiclesModel
        Log.e("city"," "+lat+" "+lng)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)

        change = findViewById(R.id.change)
        location = findViewById(R.id.location)
        next = findViewById(R.id.next)
        title.text = getString(R.string.Request_to_change_oil)
        back.setOnClickListener { onBackPressed()
            finish()}

        change.setOnClickListener { val intent = Intent(this@CompleteOilOrderActivity, ChangeOilLocationActivity::class.java)
            intent.putExtra("vehicle", myVehiclesModel)
            startActivity(intent)
        finish()}
        location.text = address
        date.setOnClickListener { startActivityForResult(Intent(this@CompleteOilOrderActivity,DateActivity::class.java),1) }
        time.setOnClickListener { startActivityForResult(Intent(this@CompleteOilOrderActivity,TimeActivity::class.java),2) }
        next.setOnClickListener {
            if (CommonUtil.checkTextError(date, getString(R.string.Select_the_date)) ||
                CommonUtil.checkTextError(time, getString(R.string.Determine_the_time))
            ) {
                return@setOnClickListener
            }else{
                addOrder()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            Log.e("onActivityResult", data?.getStringExtra("result"))
            date.text = data?.getStringExtra("result")
        }else if (resultCode == 2){
            Log.e("onActivityResult", data?.getStringExtra("time"))
            time.text = data?.getStringExtra("time")
        }
    }

    fun addOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.addOilOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,myVehiclesModel.id!!,"oil",lat,lng,address,date.text.toString(),time.text.toString(),oil_id,num,type,viscosity,oil,makena,air,have_oil)?.enqueue(
            object : Callback<AddOrderResponse> {
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@CompleteOilOrderActivity,OilInvoiceActivity::class.java)
                            intent.putExtra("order",response.body()?.data!!)
                            intent.putExtra("lat",lat)
                            intent.putExtra("lng",lng)
                            startActivity(intent)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}