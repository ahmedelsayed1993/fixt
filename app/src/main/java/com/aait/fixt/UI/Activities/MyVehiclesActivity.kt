package com.aait.fixt.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Models.MyVehiclesResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Adapters.HomeAdapter
import com.aait.fixt.UI.Adapters.MyVehicleAdapter
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyVehiclesActivity :Parent_Activity(), OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (myVehiclesModels.get(position).is_order==1){
            val intent = Intent(this@MyVehiclesActivity,VechicleDetailsActivity::class.java)
            intent.putExtra("id",myVehiclesModels.get(position).id)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_my_vehicles
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var add:ImageView
    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal var myVehiclesModels= ArrayList<MyVehiclesModel>()
    internal lateinit var myVehicleAdapter: MyVehicleAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        add = findViewById(R.id.add)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoItem = findViewById(R.id.lay_no_item)
        layNoInternet = findViewById(R.id.lay_no_internet)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        myVehicleAdapter = MyVehicleAdapter(mContext,myVehiclesModels,R.layout.recycler_vehicles)
        myVehicleAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = myVehicleAdapter
        back.setOnClickListener { startActivity(Intent(this@MyVehiclesActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.my_cars)
        add.setOnClickListener { startActivity(Intent(this@MyVehiclesActivity,AddVehicleActivity::class.java))
        finish()}
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getCars() }
        getCars()

    }

    fun getCars(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.myVehicles(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object : Callback<MyVehiclesResponse> {
                override fun onFailure(call: Call<MyVehiclesResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<MyVehiclesResponse>,
                    response: Response<MyVehiclesResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            }else{

                                myVehicleAdapter.updateAll(response.body()?.data!!)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}