package com.aait.fixt.UI.Activities

import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.Models.BaseResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_change_password
    lateinit var current_password:EditText
    lateinit var new_password:EditText
    lateinit var confirm_new_pass:EditText
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var confirm:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        current_password = findViewById(R.id.current_password)
        new_password = findViewById(R.id.new_password)
        confirm_new_pass = findViewById(R.id.confirm_new_pass)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.change_password)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(
                    current_password!!,
                    getString(R.string.current_password)
                ) ||
                CommonUtil.checkEditError(
                    new_password!!,
                    getString(R.string.new_password)
                ) ||
                CommonUtil.checkLength(new_password, getString(R.string.password_length), 6) ||
                CommonUtil.checkEditError(
                    confirm_new_pass!!,
                    getString(R.string.confirm_new_password)
                )
            ) {
                return@setOnClickListener
            } else {
                if (!confirm_new_pass.text.toString().equals(new_password.text.toString())) {
                    confirm_new_pass.error = getString(R.string.password_not_match)
                } else {
                      change()
                }
            }
        }


    }

    fun change(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resetPassword(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,current_password.text.toString(),new_password.text.toString())?.enqueue(object :Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}