package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.TimePicker
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R
import com.aait.fixt.Uitls.CommonUtil

class ToTimeActivity : Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_time
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var confirm: Button
    var time = ""


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Determine_the_time)
        OnClickTime()
        confirm.setOnClickListener {
            if (time.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.Determine_the_time))
            }else {
                val returnIntent = Intent()
                returnIntent.putExtra("time", time)
                setResult(3, returnIntent)
                finish()
            }
        }


    }
    private fun OnClickTime() {

        val timePicker = findViewById<TimePicker>(R.id.timePicker)
        timePicker.setOnTimeChangedListener { _, hour, minute ->
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }

            val hours = if (hour < 10) "0" + hour else hour
            val min = if (minute < 10) "0" + minute else minute
            // display format of time
            time = " $hours $am_pm"


        }
    }
}