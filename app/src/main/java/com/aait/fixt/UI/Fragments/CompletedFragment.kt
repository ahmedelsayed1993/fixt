package com.aait.fixt.UI.Fragments

import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.fixt.Base.BaseFragment
import com.aait.fixt.Client
import com.aait.fixt.Models.OrderModel
import com.aait.fixt.Models.OrdersResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Adapters.FinishedAdapter
import com.aait.fixt.UI.Adapters.OrderAdapter
import com.aait.fixt.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompletedFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_current
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderModels = java.util.ArrayList<OrderModel>()
    internal lateinit var orderAdapter: FinishedAdapter


    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        orderAdapter = mContext?.let { FinishedAdapter(it, orderModels, R.layout.recycler_finish_order) }!!

        rv_recycle!!.layoutManager = linearLayoutManager
        rv_recycle!!.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }

    fun getHome() {
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)
            ?.myOrder(mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!, "completed")
            ?.enqueue(object :
                Callback<OrdersResponse> {
                override fun onResponse(
                    call: Call<OrdersResponse>,
                    response: Response<OrdersResponse>
                ) {
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        try {
                            if (response.body()?.value.equals("1")) {
                                Log.e("myJobs", Gson().toJson(response.body()!!.data))
                                if (response.body()!!.data?.isEmpty()!!) {
                                    layNoItem!!.visibility = View.VISIBLE
                                    layNoInternet!!.visibility = View.GONE
                                    tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                                } else {
//                            initSliderAds(response.body()?.slider!!)
                                    orderAdapter.updateAll(response.body()!!.data!!)
                                }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                            } else {
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {
                    }
                }

                override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                    mContext?.let { CommonUtil.handleException(it, t) }
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                    Log.e("response", Gson().toJson(t))
                }
            })
    }
}