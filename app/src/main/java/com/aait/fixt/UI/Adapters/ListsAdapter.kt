package com.aait.fixt.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.fixt.Base.ParentRecyclerAdapter
import com.aait.fixt.Base.ParentRecyclerViewHolder
import com.aait.fixt.Models.ListModel
import com.aait.fixt.Models.VehicleModel
import com.aait.fixt.R

class ListsAdapter (context: Context, data: MutableList<VehicleModel>, layoutId: Int) :
    ParentRecyclerAdapter<VehicleModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val vehicleModel = data.get(position)
        viewHolder.name!!.setText(vehicleModel.name)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)


    }
}