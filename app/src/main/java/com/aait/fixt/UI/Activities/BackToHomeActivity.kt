package com.aait.fixt.UI.Activities

import android.content.Intent
import android.widget.TextView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.R

class BackToHomeActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_back_to_home
    lateinit var back:TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this@BackToHomeActivity,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@BackToHomeActivity,MainActivity::class.java))
        finish()
    }
}