package com.aait.fixt.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.fixt.Base.Parent_Activity
import com.aait.fixt.Client
import com.aait.fixt.GPS.GPSTracker
import com.aait.fixt.GPS.GpsTrakerListener
import com.aait.fixt.Listeners.OnItemClickListener
import com.aait.fixt.Models.AboutResponse
import com.aait.fixt.Models.AddOrderResponse
import com.aait.fixt.Models.MyVehiclesModel
import com.aait.fixt.Models.MyVehiclesResponse
import com.aait.fixt.Network.Service
import com.aait.fixt.R
import com.aait.fixt.UI.Adapters.HomeAdapter
import com.aait.fixt.Uitls.CommonUtil
import com.aait.fixt.Uitls.DialogUtil
import com.aait.fixt.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_add_vehicle.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.recycler_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class MainActivity:Parent_Activity(),GoogleMap.OnMarkerClickListener, OnMapReadyCallback,
    GpsTrakerListener,OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.check){
            myVehiclesModel = myVehiclesModels.get(position)
            vehicle = myVehiclesModel.vehicle!!
            Log.e("vehicle",Gson().toJson(myVehiclesModel))
        }else {
            myVehiclesModel = myVehiclesModels.get(position)
            vehicle = myVehiclesModel.vehicle!!
            Log.e("vehicle", Gson().toJson(myVehiclesModel))
        }


    }

    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var add_vehicle:TextView
    lateinit var map:MapView
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var vehicle = ""
    lateinit var menu:ImageView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var main:TextView
    lateinit var my_cars:TextView
    lateinit var profile:TextView
    lateinit var my_orders:TextView
    lateinit var contact_us:TextView
    lateinit var about_app:TextView
    lateinit var logout:TextView

    internal lateinit var markerOptions: MarkerOptions
    private var mAlertDialog: AlertDialog? = null
    lateinit var cars:RecyclerView
    lateinit var myVehiclesModel: MyVehiclesModel
     internal var myVehiclesModels= ArrayList<MyVehiclesModel>()
    internal lateinit var homeAdapter: HomeAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var notification:ImageView
    lateinit var no:TextView
    lateinit var oil:LinearLayout
    lateinit var wash:LinearLayout
    lateinit var one:TextView
    lateinit var two:TextView
    lateinit var three:TextView
    lateinit var four:TextView
    lateinit var five:TextView
    lateinit var six:TextView
    lateinit var time:TextView
    lateinit var lang:TextView
    var deviceID=""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()

        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        five = findViewById(R.id.five)
        six = findViewById(R.id.six)
        map = findViewById(R.id.map)
        oil = findViewById(R.id.oil)
        time = findViewById(R.id.time)
        notification = findViewById(R.id.notification)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)

        if (timeOfDay >= 0 && timeOfDay < 12) {
            time.text = getString(R.string.good_morning)+"  "+ mSharedPrefManager.userData.name
        } else if (timeOfDay >= 12 && timeOfDay < 24) {
            time.text = getString(R.string.good_evening)+"  "+ mSharedPrefManager.userData.name
        }

        notification.setOnClickListener { startActivity(Intent(this@MainActivity,NotificationActivity::class.java))
        finish()}
        cars = findViewById(R.id.cars)
        no = findViewById(R.id.no)
        add_vehicle = findViewById(R.id.add)
        menu = findViewById(R.id.menu)
        wash = findViewById(R.id.wash)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        homeAdapter = HomeAdapter(mContext,myVehiclesModels,R.layout.recycler_main)
        homeAdapter.setOnItemClickListener(this)
        cars.layoutManager = linearLayoutManager
        cars.adapter = homeAdapter
        add_vehicle.setOnClickListener { startActivity(Intent(this@MainActivity,AddVehicleActivity::class.java)) }
        sideMenu()
        getCars()

        oil.setOnClickListener {
            if (vehicle.equals("")){

            }else {
                val intent = Intent(this@MainActivity, ChangeOilLocationActivity::class.java)
                intent.putExtra("vehicle", myVehiclesModel)
                startActivity(intent)
            }}
        wash.setOnClickListener {
            if (vehicle.equals("")){

            }else {
                val intent = Intent(this@MainActivity, CarWashActivity::class.java)
                intent.putExtra("vehicle", myVehiclesModel)
                startActivity(intent)
            }}

        getNum()

    }
    fun getNum(){
        Client.getClient()?.create(Service::class.java)?.number(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object :Callback<AddOrderResponse>{
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()

                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data==1){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                           else if (response.body()?.data==2){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                            else if (response.body()?.data==3){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                            else if (response.body()?.data==4){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                            else if (response.body()?.data==5){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                            else if (response.body()?.data==6){
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.green_circle)
                            }else{
                                one.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                two.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                three.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                four.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                five.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                                six.background = ContextCompat.getDrawable(mContext,R.drawable.white_circle)
                            }
                        }
                    }
                }

            }
        )
    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 25f)
        drawer_layout.setRadius(Gravity.END, 25f)
        drawer_layout.setViewScale(Gravity.START, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewScrimColor(Gravity.START, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 20f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 20f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 40f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 40f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        logout = drawer_layout.findViewById(R.id.logout)
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        main = drawer_layout.findViewById(R.id.main)
        about_app = drawer_layout.findViewById(R.id.about_app)
        my_cars = drawer_layout.findViewById(R.id.my_cars)
        my_orders = drawer_layout.findViewById(R.id.my_orders)
        profile = drawer_layout.findViewById(R.id.profile)
        lang = drawer_layout.findViewById(R.id.lang)


        contact_us = drawer_layout.findViewById(R.id.contact_us)

        Glide.with(mContext).load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name

        main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        profile.setOnClickListener { startActivity(Intent(this@MainActivity,ProfileActivity::class.java))
        finish()}
        my_cars.setOnClickListener { startActivity(Intent(this@MainActivity,MyVehiclesActivity::class.java))
        finish()}
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java))
        finish()}
        my_orders.setOnClickListener { startActivity(Intent(this@MainActivity,OrdersActivity::class.java))
        finish()}
        contact_us.setOnClickListener { startActivity(Intent(this@MainActivity,ContactUsActivity::class.java))
        finish()}
        lang.setOnClickListener { startActivity(Intent(this@MainActivity,LanguageActivity::class.java))
            finish()}
//        about_app.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,AboutAppActivity::class.java)) }
//        terms.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,TermsActivity::class.java))  }
//        repeated.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,RepeatedQuestionsActivity::class.java)) }
//        contact_us.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ContactUsActivity::class.java)) }
//        complains.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ComplainsActivity::class.java)) }
//        share_app.setOnClickListener { CommonUtil.ShareApp(applicationContext) }
//        Financial_accounts.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,FinancialActivity::class.java)) }
//
        logout.setOnClickListener { logout() }
    }
    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
//        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this)
        getLocationWithPermission()
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }


    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                       // mResult = addresses[0].getAddressLine(0)

                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }

    fun getCars(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.myVehicles(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object : Callback<MyVehiclesResponse> {
                override fun onFailure(call: Call<MyVehiclesResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<MyVehiclesResponse>,
                    response: Response<MyVehiclesResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                cars.visibility = View.GONE
                                no.visibility = View.VISIBLE
                            }else{
                                cars.visibility = View.VISIBLE
                                no.visibility = View.GONE
                                myVehiclesModel = response.body()?.data?.get(0)!!
                                vehicle = myVehiclesModel.vehicle!!
                                homeAdapter.updateAll(response.body()?.data!!)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }

    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutResponse> {
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutResponse>,
                response: Response<AboutResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(applicationContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(applicationContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}