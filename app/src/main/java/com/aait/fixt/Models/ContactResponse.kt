package com.aait.fixt.Models

import java.io.Serializable

class ContactResponse:BaseResponse(),Serializable {

    var phones:PhonesModel?=null
    var data:ArrayList<SocialModel>?=null
}