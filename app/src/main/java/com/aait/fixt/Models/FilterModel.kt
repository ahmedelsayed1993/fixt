package com.aait.fixt.Models

import java.io.Serializable

class FilterModel:Serializable {
    var id:String?=null
    var name:String?=null

    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }
}