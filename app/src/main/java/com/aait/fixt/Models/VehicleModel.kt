package com.aait.fixt.Models

import java.io.Serializable

class VehicleModel:Serializable {
    var id:Int?=null
    var name:String?=null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }
}