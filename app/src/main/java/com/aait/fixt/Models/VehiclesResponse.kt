package com.aait.fixt.Models

import java.io.Serializable

class VehiclesResponse:BaseResponse(),Serializable {
    var data:ArrayList<VehicleModel>?=null
}