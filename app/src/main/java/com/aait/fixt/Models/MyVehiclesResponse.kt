package com.aait.fixt.Models

import java.io.Serializable

class MyVehiclesResponse:BaseResponse(),Serializable {
    var data:ArrayList<MyVehiclesModel>?=null
}