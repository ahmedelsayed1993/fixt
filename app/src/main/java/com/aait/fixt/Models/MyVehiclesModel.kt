package com.aait.fixt.Models

import java.io.Serializable

class MyVehiclesModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var vehicle:String?=null
    var plate_number:String?=null
    var plate_letters:String?=null
    var km_number:String?=null
    var date_change:String?=null
    var is_order:Int?=null
    var selected:Boolean?=null
}