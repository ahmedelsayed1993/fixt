package com.aait.fixt.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var service:String?=null
    var total:String?=null
    var time:String?=null
    var date:String?=null
    var km_number:String?=null
}